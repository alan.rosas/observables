﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class ExampleMethods : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Si tenes dudas de como se usan algunos operadores aca tenes una pista extra
        
        ReturnBoolObservable()
            .Do(valueEmitted => Debug.Log(valueEmitted))
            .DoOnError(exception=>Debug.Log(exception))
            .Subscribe();

        ReturnUnitObservable()
            .DoOnSubscribe(() => Debug.Log(""))
            .DoOnCompleted(() =>
            {
                Debug.Log("Se puede");
                Debug.Log("Agregar un");
                Debug.Log("Bloque de codigo");
                Debug.Log("Tanto en el Do,DoOnSubscribe,DoOnError y DoOnComplete");
            }).Subscribe();


        ReturnUnitObservable()
            .First()
            .Subscribe();
        
        ReturnUnitObservable()
            .Last()
            .Subscribe();
        
        ReturnUnitObservable()
            .Concat(ReturnUnitObservable())
            .Subscribe();
        
        ReturnUnitObservable()
            .Merge(ReturnUnitObservable())
            .Subscribe();

        ReturnBoolObservable()
            .Select(valueBool => !valueBool)
            .Subscribe();
        
        ReturnListBoolObservable()
            .SelectMany(list => list)
            .Do(value=>Debug.Log(value))
            .Subscribe();

        ReturnUnitObservable()
            .Delay(TimeSpan.FromSeconds(2))
            .Do(_ => Debug.Log("Como parametro se puso _ porque no lo estoy usando y espere 2 segundos"))
            .Subscribe();

        Observable.Interval(TimeSpan.FromSeconds(2))
            .Throttle(TimeSpan.FromSeconds(1))
            .Subscribe();
        
        Observable.Interval(TimeSpan.FromSeconds(2))
            .Take(10)
            .Do(seg=>Debug.Log(seg))
            .Subscribe();

        ReturnListBoolObservable()
            .ContinueWith(ReturnListIntObservable())
            .Subscribe();

        Observable.WhenAll(new[]
        {
            ReturnUnitObservable().Delay(TimeSpan.FromSeconds(2)),
            ReturnUnitObservable().Delay(TimeSpan.FromSeconds(5)),
            ReturnUnitObservable().Delay(TimeSpan.FromSeconds(1)),
            ReturnUnitObservable().Delay(TimeSpan.FromSeconds(8)),
        })
            .Do(_ => Debug.Log("Se espero a que se completen todos para seguir"))
            .Subscribe();
    }

    private IObservable<Unit> ReturnUnitObservable()
    {
        return Observable.ReturnUnit();
        return Observable.Create<Unit>(emitter =>
        {
            emitter.OnNext(Unit.Default);
            emitter.OnCompleted();
            return Disposable.Empty;
        });
    }
    
    private IObservable<bool> ReturnBoolObservable()
    {
        return Observable.Create<bool>(emitter =>
        {
            emitter.OnNext(true);
            emitter.OnCompleted();
            return Disposable.Empty;
        });
        return Observable.Return<bool>(true);
    }
    
    private IObservable<List<bool>> ReturnListBoolObservable()
    {
        return Observable.Create<List<bool>>(emitter =>
        {
            var list = new List<bool>();
            list.Add(true);
            list.Add(false);
            list.Add(true);
            emitter.OnNext(list);
            emitter.OnCompleted();
            return Disposable.Empty;
        });
    }
    
    private IObservable<List<int>> ReturnListIntObservable()
    {
        return Observable.Create<List<int>>(emitter =>
        {
            var list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            emitter.OnNext(list);
            emitter.OnCompleted();
            return Disposable.Empty;
        });
    }

    private IObservable<long> UseInterval()
    {
        return Observable.Interval(TimeSpan.FromSeconds(1));
    }
}

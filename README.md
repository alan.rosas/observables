Ejercicios de Practicas, si no sabe como avanzar, relea la diapositiva,en caso de seguir atascado pregunte a alguien de su equipo.1

Cree un script que herede de Monobehaviour y use el start para su subscripcion

1-Crear un observable que te devuelva un numero y poder imprimirlo en pantalla.

2-Haga que al subscribirse se imprima una frase como "Se suscribio" y que al completarse diga "Completo".

3-Cambie el observable para que ahora retorne una lista de palabras cumpliendo lo del punto 2 todavia.(Puede recibirla por parametros o crearla dentro a la lista)

4-Cree otro observable que sirva como cuenta regresiva,ahora puede usarse el punto 2 pero con un "Preparado" 
seguido de la cuenta regresiva de 3 segs y el "salida" cuando este completo

5-Usar lo del punto 4,pero ahora al completarse se debe mostrar el contenido de la lista 3